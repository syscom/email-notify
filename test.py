import asyncio
import functools
import logging
import smtplib
import threading
from unittest import TestCase
from unittest.mock import patch, Mock, call

from aiosmtpd.smtp import SMTP

from email_notify import MatrixHandler, SMTPHandler

MATRIX_HOST = "https://localhost"
MATRIX_USERNAME = "@piet:localhost"
MATRIX_TOKEN = "something"
MATRIX_ROOM = "test:localhost"

RECIPIENT_NAME = "notifications"
RECIPIENT = "{}@example.org".format(RECIPIENT_NAME)
SOURCE = "source@example.com"
SUBJECT = "Hi there"
DEFAULT_EMAIL = """\
From: {}
To: {}
Subject: {}

Hello!""".format(SOURCE, RECIPIENT, SUBJECT)

EMAIL_RECIPIENTS = [RECIPIENT, "notify@example.org"]
EMAIL_LISTEN = "127.0.0.1"
EMAIL_PORT = 8025


class TestNotify(TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        logging.basicConfig(level=logging.WARNING)

    def setUp(self):
        self.matrix_client_send = Mock(name="matrix_client_send", )
        self.matrix_client_room = Mock(name="matrix_client_room",
                                       return_value=Mock(
                                           send_html=self.matrix_client_send))
        self.matrix_client = Mock(name="matrix_client",
                                  join_room=self.matrix_client_room)
        self.matrix_client_class = Mock(name="matrix_client_class",
                                        return_value=self.matrix_client)
        patch("email_notify.MatrixClient",
              new=self.matrix_client_class).start()

        self.loop = asyncio.new_event_loop()
        matrix_handler = MatrixHandler(MATRIX_HOST, username=MATRIX_USERNAME,
                                       token=MATRIX_TOKEN)
        matrix_handler.join_room(MATRIX_ROOM)
        smtp_handler = SMTPHandler(matrix_handler.handle_email,
                                   EMAIL_RECIPIENTS)
        self.factory = functools.partial(SMTP, smtp_handler,
                                         enable_SMTPUTF8=True)
        self.server = self.loop.run_until_complete(self.loop.create_server(
            self.factory, host=EMAIL_LISTEN, port=EMAIL_PORT))
        self.server_thread = threading.Thread(target=self.loop.run_forever)
        self.server_thread.start()

    def tearDown(self):
        self.loop.call_soon_threadsafe(self.server.close)
        self.loop.call_soon_threadsafe(self.server.wait_closed)
        self.loop.call_soon_threadsafe(self.loop.stop)
        self.server_thread.join()
        self.loop.close()
        patch.stopall()

    def test_initialisation(self):
        self.assertEqual(
            self.matrix_client_class.call_args,
            call(MATRIX_HOST, token=MATRIX_TOKEN, user_id=MATRIX_USERNAME)
        )
        self.assertEqual(self.matrix_client_room.call_args, call(MATRIX_ROOM))
        self.assertFalse(self.matrix_client_send.called)

    def test_valid_mail(self):
        server = smtplib.SMTP(EMAIL_LISTEN, EMAIL_PORT)
        server.sendmail(SOURCE, RECIPIENT, DEFAULT_EMAIL)
        server.close()
        self.assertTrue(self.matrix_client_send.called)

        html = self.matrix_client_send.call_args[0][0]
        self.assertTrue(RECIPIENT_NAME in html)
        self.assertTrue(SOURCE in html)
        self.assertTrue(SUBJECT in html)

        body = self.matrix_client_send.call_args[1]['body']
        self.assertTrue(RECIPIENT_NAME in body)
        self.assertTrue(SOURCE in body)
        self.assertTrue(SUBJECT in body)
