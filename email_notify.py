#!/usr/bin/env python3
# coding: utf-8

import argparse
import asyncio
import configparser
import email
import functools
import html
import logging
import signal
from email.header import decode_header, make_header

from aiosmtpd.smtp import SMTP, Envelope
from matrix_client.api import MatrixRequestError
from matrix_client.client import MatrixClient
from requests.exceptions import MissingSchema

HTML_MESSAGE = "\U0001f4e9 <font color=\"blue\"><b>{}:</b></font> “{}” – “{}”"

PLAIN_MESSAGE = "\U0001f4e9 {}: “{}” – “{}”"


class MatrixHandler:
    """Handles the posting of the notifications in the Matrix rooms"""

    def __init__(self, host: str, username: str, token: str):
        self.rooms = []
        try:
            self.client = MatrixClient(host, user_id=username, token=token)
        except MatrixRequestError as e:
            raise RuntimeError("Check if your server details are correct.") \
                from e
        except MissingSchema as e:
            raise ValueError("Bad URL format.") from e

    def join_room(self, room: str):
        try:
            new_room = self.client.join_room(room)
        except MatrixRequestError as e:
            if e.code == 400:
                raise ValueError("Room ID/Alias in the wrong format.") from e
            else:
                raise RuntimeError("Couldn't find room.") from e
        self.rooms.append(new_room)

    def handle_email(self, from_header: str, subject_header: str,
                     envelope: Envelope):
        headers = email.message_from_bytes(envelope.content)

        to = ''
        try:
            to = str(headers['To']).split('@')[0]
        except IndexError:
            logging.exception("Malformed email address")
        except KeyError:
            logging.exception("Missing 'To' header")

        message = HTML_MESSAGE.format(html.escape(to),
                                      html.escape(from_header),
                                      html.escape(subject_header))
        plain_message = PLAIN_MESSAGE.format(to, from_header, subject_header)
        for room in self.rooms:
            room.send_html(message, body=plain_message, msgtype="m.notice")


class SMTPHandler:
    """
    Handles SMTP messages and passes the subject and sender to another handler
    """

    def __init__(self, handler=None, recipients=[]):
        """Constructs a new SMTPHandler

        :param handler:     An object that responds to
                            `handle_email(from, subj, envelope)`, or `None`
        :param recipients:  An iterable containing valid recipient addresses
        """
        self.handler = handler
        self.recipients = recipients

    async def handle_RCPT(self, server, session, envelope,
                          address, recipient_options):
        if address in self.recipients:
            envelope.rcpt_tos.append(address)
            return '250 OK'
        else:
            logging.warning("Incorrect recipient address {}".format(address))
            return '550 Bad destination address'

    async def handle_DATA(self, server, session, envelope: Envelope):
        headers = email.message_from_bytes(envelope.content)
        from_header = ''
        subject_header = ''
        if 'subject' in headers:
            subject = headers['subject']
            subject_header = make_header(decode_header(subject))
        if 'from' in headers:
            from_header = headers['from']
        logging.debug('Mail from "{}" subject "{}"'.format(from_header,
                                                           subject_header))
        if self.handler:
            try:
                self.handler(str(from_header), str(subject_header), envelope)
            except Exception:
                logging.exception("Handler raised an exception")
        return '250 OK'


class NotifierConfig:
    """Email notifier configuration class.

    This class is used to parse and store all the configuration for the email
    notifier.
    """

    def __init__(self, config_file):
        """Creates a new configuration object.

        :param config_file: Path to the configuration file.
        """
        self.config_file = config_file
        parser = configparser.ConfigParser()
        parser.read(self.config_file)

        try:
            self.matrix_host = parser.get('matrix', 'homeserver')
            self.matrix_username = parser.get('matrix', 'user')
            self.matrix_token = parser.get('matrix', 'token')
            self.matrix_room = parser.get('matrix', 'room')

            self.email_recipients = parser.get('email',
                                               'recipients_allowed').split()
            self.email_listen = parser.get('email', 'address')
            self.email_port = parser.get('email', 'port')
        except configparser.Error as e:
            raise RuntimeError('Configuration issues detected in {}: {}'
                               .format(self.config_file, e)) from e


def main():
    logging.basicConfig(level=logging.WARNING)

    parser = argparse.ArgumentParser(description='Email to Matrix notifier'
                                                 ' daemon')

    parser.add_argument('config', type=str)
    args = parser.parse_args()

    config = NotifierConfig(args.config)

    loop = asyncio.get_event_loop()

    matrix_handler = MatrixHandler(config.matrix_host,
                                   username=config.matrix_username,
                                   token=config.matrix_token)
    matrix_handler.join_room(config.matrix_room)
    smtp_handler = SMTPHandler(matrix_handler.handle_email,
                               config.email_recipients)

    factory = functools.partial(SMTP, smtp_handler, enable_SMTPUTF8=True)

    server = loop.run_until_complete(loop.create_server(
        factory, host=config.email_listen, port=config.email_port,
    ))
    loop.add_signal_handler(signal.SIGINT, loop.stop)
    try:
        loop.run_forever()
    except KeyboardInterrupt:
        pass
    server.close()
    loop.run_until_complete(server.wait_closed())
    loop.close()


if __name__ == '__main__':
    main()
